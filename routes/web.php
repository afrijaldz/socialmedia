<?php
Route::group(['middleware' => ['web']], function(){

  Route::post('signup', [ 'uses' => 'UserController@postSignUp', 'as' => 'signup' ]);

  Route::post('signin', [ 'uses' => 'UserController@postSignIn', 'as' => 'signin' ]);

  Route::get('/', [ 'uses' => 'UserController@welcome', 'as' => 'welcome' ]);

  Route::get('dashboard', [ 'uses' => 'UserController@dashboard', 'as' => 'dashboard' ]);

});
